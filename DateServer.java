// --------------------------------------------------------------------------------------- //
//
//  @Author:        Vanzo Luca Samuele
//  @Version:       1.0.100
//  @Revision:      1
//
//  Semplice applicativo JAVA Server Multi-Client per erogazione di data e frasi.
//  Nel sorgente sono presenti esempi di THREAD, SYNCH, WAIT, NOTIFY, LOAD SCALING...
//  Codice di esempio per il corso "Reti e Sistemi Operativi".
//
// --------------------------------------------------------------------------------------- //

import java.net.*;
import java.io.*;
import java.util.*;

// --------------------------------------------------------------------------------------- //
//  Classe entry-point
// --------------------------------------------------------------------------------------- //

public class DateServer extends Thread
{
    // Indicare la PORTA di ascolto per il server.
    private final int LISTEN_PORT   = 8000;

    // Indicare il numero di THREAD da utilizzare per i clients.
    // Normalmente il valore ideale è uguale al numero di (CORE - 1) logici della CPU,
    // nel caso di ambienti virtualizzati al numero di (vCORE - 1). Uno per il LISTENER.
    // La scalabilità permette di aumentare la capacità di rispondere alla richiesta
    // dietro il variare delle risorse fisiche/virtuali della macchina/vps.
    private final int NUM_WORKER    = 4;

    // Oggetti utili: listener, storage clients e workers.
    private ServerSocket _listener  = null;
    private ClientStorage _clients  = null;
    private ClientWorker[] _workers = null;

    // ENTRY POINT PROGRAMMA
    public static void main(String[] args)
    {
        try
        {
            // Stampo intestazione inutilmente utile (è un ossimoro) XD
            System.out.println("\n Applicativo SO & Reti - JAVA Server (Demo App.)");
            System.out.println(" Produttore: Vanzo Luca Samuele, U14 Univ. Milano Bicocca - 2015\n");

            // Inizializzo oggetto server e input tastiera.
            DateServer handler = new DateServer();
            Scanner keyboard   = new Scanner(System.in);

            // Avvio il server.
            handler.start();

            // Procedura chiusura server.
            System.out.println(" Press any keys to shutdown...\n");
            keyboard.nextLine();

            // Chiudo listener, thread e server.
            handler.shutdown();
        }
        catch (Exception e)
        {
            // Stampo possibili errori.
            System.out.println(" SERVER: " + e);
        }
    }

    // COSTRUTTORE
    public DateServer()
    {
        // Inizializzo lo storage dei clients (gestisce la synch. dei worker).
        this._clients  = new ClientStorage();

        // Inizializzo il vettore worker (array).
        this._workers  = new ClientWorker[NUM_WORKER];

        // Inizializzo ed avvio le classi worker + unico ID per stampa.
        for(int i = 0; i < NUM_WORKER; i++)
        {
            // Essendo un vettore di classi è necessario inizializzare ogni elemento.
            this._workers[i] = new ClientWorker(i, this._clients);

            // Avvio il thread worker nella i-esima posizione.
            this._workers[i].start();
        }
    }

    // PROCEDURE DI CHIUSURA
    public void shutdown()
    {
        try
        {
            // Chiudo in primis il listener.
            // Altrimenti potrei avere client nuovi post clear dello storage!
            this._listener.close();

            // Ciclo di vita.
            while(true)
            {
                // Ottengo sempre il primo client (ottenere = rimuovere).
                // Controllo che sia diverso da NULL (flag di empty).
                Socket tmp = this._clients.getJob();
                if(tmp == null) break;

                // Stampo informazioni socket
                String hostAndPort  = "IP-ADDR[ " + tmp.getInetAddress().getHostAddress() + ":" + tmp.getPort() + " ]";
                System.out.println(" SERVER: Shutdown client connection -> " + hostAndPort);

                // Chiudo la connessione lato server.
                tmp.close();
            }

            // Termino tutti i worker.
            for(int i = 0; i < NUM_WORKER; i++)
            {
                // Chiudo ogni thread worker.
                System.out.println(" SERVER: Shutdown thread-worker... [ ID" + i + " ]");
                this._workers[i].interrupt();
            }
        }
        catch (Exception e)
        {
            // Stampo possibili errori.
            System.out.println(" SERVER: " + e);
        }
    }

    // THREAD PRINCIPALE
    public void run()
    {
        try
        {
            // Avvio il server in ascolto sulla porta LISTEN_PORT.
            System.out.println(" SERVER: Bind on PORT[ " + LISTEN_PORT + " ]...");
            _listener = new ServerSocket(LISTEN_PORT);
            System.out.println(" SERVER: Ready! Waiting...\n");

            // Ciclo di vita.
            while(!Thread.currentThread().isInterrupted())
            {
                // Salvo temporaneamente il client pendente.
                Socket clientSock  = this._listener.accept();

                // Stampa informazioni dal thread listener.
                String hostAndPort = "IP-ADDR[ " + clientSock.getInetAddress().getHostAddress() + ":" + clientSock.getPort() + " ]";
                System.out.println(" Thread-Listener[ " + LISTEN_PORT + " ]->Accept:  " + hostAndPort);

                // Aggiunto allo storage dei client la nuova connessione ACCETTATA.
                this._clients.addJobs(clientSock);
            }
        }
        catch (Exception e)
        {
            // Stampo possibili errori.
            System.out.println(" SERVER: Shutdown socket listener...");
        }
    }
}

// --------------------------------------------------------------------------------------- //
//  Classe contenitore (synch.) per elaborazione client connessi mediante Pool-Thread
// --------------------------------------------------------------------------------------- //

class ClientStorage
{
    // Dichiarazione lista socket client.
    private ArrayList<Socket> _sockets;

    // COSTRUTTORE
    public ClientStorage()
    {
        // Inizializzazione lista socket client.
        _sockets = new ArrayList<Socket>();
    }

    // Aggiunta di un nuovo client alla lista.
    // Il metodo è synch. poichè usato da più thread contemporaneamente.
    // Il trimtosize è utile per mantenere l'utilizzo della memoria al "necessario".
    // Infine notifico al primo thread disponibile (ready) la presenza di nuovi client.
    public synchronized void addJobs(Socket sock)
    {
        // Aggiunta client.
        _sockets.add(sock);
        _sockets.trimToSize();

        // Risveglio il primo thread utile.
        notify();
    }

    // Controllo dimensione lista.
    // Nel caso in cui dimensione = ZERO cambio stato al thread.
    // Lo stato di WAIT è rimosso dal NOTIFY.
    public synchronized boolean Empty() throws InterruptedException
    {
        // Controllo messa in WAIT
        if(_sockets.isEmpty())
        {
            // Metto il thread in attesa.
            wait();
        }

        // Ritorno se dimensione è uguale o meno a ZERO.
        return _sockets.isEmpty();
    }

    // Ottengo il primo socket client dalla lista.
    // La rimozione è dovuta poichè al termine di ogni esecuzione il
    // thread impilerà o meno (es: se ricevo un quit) nella lista il client.
    // Questa pratica permette di risolvere alcuni sgradevoli imprevisti dovuti
    // alla navigazione per indice (vedi rimozione ed aggiunta durante navigazione).
    public synchronized Socket getJob()
    {
        // Controllo dimensione lista.
        if(_sockets.isEmpty())
            return null;

        // Ottengo il client in testa alla coda.
        Socket tmp = _sockets.get(0);

        // Rimuovo il client estratto dalla lista e calcolo la dimensione.
        _sockets.remove(0);
        _sockets.trimToSize();

        // Ritorno il client al worker.
        return tmp;
    }
}

// --------------------------------------------------------------------------------------- //
//  Classe lavoratore (thread-worker) per elaborazione richieste client
// --------------------------------------------------------------------------------------- //

class ClientWorker extends Thread
{
    // ID stampa del worker.
    private int threadID;

    // Gestore dello storage.
    private ClientStorage handlerStorage;

    // COSTRUTTORE
    public ClientWorker(int idt, ClientStorage obj)
    {
        // Inizializzazioni varie.
        threadID        = idt;
        handlerStorage  = obj;
    }

    // THREAD PRINCIPALE
    public void run()
    {
        try
        {
            // Ciclo di vita del thread worker.
            while(!Thread.currentThread().isInterrupted())
            {
                // CIclo condizionato per WAIT.
                // In caso di notify successive evita il blocco del thread
                // per exception dovute al consumo di client nulli (lista vuota).
                while(handlerStorage.Empty());

                // Ottengo dallo storage il lavoro (client).
                Socket curSocket = handlerStorage.getJob();

                // Verifico l'esistenza del client e connessione di rete.
                if(curSocket != null && !curSocket.isClosed() && curSocket.isConnected())
                {
                    // Ottengo il canale INPUT del socket (creo una stampa di IP e PORTA).
                    InputStream isin        = curSocket.getInputStream();
                    String hostAndPort      = "IP-ADDR[ " + curSocket.getInetAddress().getHostAddress() + ":" + curSocket.getPort() + " ]";

                    // Rendo il socket NON-Bloccante per il solo I/O.
                    // Altrimenti il thread non potrebbe consumare ciclicamente i client dello storage!!!
                    if(isin.available() > 0)
                    {
                        // Stampa informazioni thread.
                        System.out.println(" Thread-Worker[ ID" + threadID + " ]->Processing:  " + hostAndPort);

                        // Ottengo il canale di OUTPUT del socket.
                        PrintWriter pout    = new PrintWriter(curSocket.getOutputStream(), true);

                        // Leggo MSG_OPCODE dal client.
                        BufferedReader brin = new BufferedReader(new InputStreamReader(isin));
                        String cmd          = brin.readLine();

                        // Cascata di confronto per MSG_OPCODE.
                        // Accedo allo scopo in presenza di GET_DATE.
                        if(cmd.compareTo("GET_DATE") == 0)
                        {
                            // Invio data ed ora corrente al client.
                            pout.println(new java.util.Date().toString());
                        }
                        // Accedo allo scopo in presenza di GET_PHRASE.
                        else if(cmd.compareTo("GET_PHRASE") == 0)
                        {
                            // Invio una frase al client.
                            pout.println("Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
                        }
                        // Accedo allo scopo in presenza di QUIT.
                        else if(cmd.compareTo("QUIT") == 0)
                        {
                            // Chiudo il socket lato server e SKIP delle ultime istruzioni (aggiunta alla coda).
                            System.out.println(" Thread-Worker[ ID" + threadID + " ]->Removing:  " + hostAndPort);
                            curSocket.close();

                            // Evito che il ciclo corrente finisca passando al successivo.
                            // In questo modo il client appena chiuso non verrà elaborato in futuro.
                            continue;
                        }
                        // Accedo allo scopo in presenza di UNKWN.
                        else if(cmd.compareTo("UNKWN") == 0)
                        {
                            // Invio messaggio di errore.
                            pout.println("Command not found!!!");
                        }
                    }

                    // Aggiungo, solo al termine del ciclo, nuovamente il client alla coda dei lavori.
                    // L'elaborazione verrà nuovamente ripresa dal primo thread worker disponibile.
                    handlerStorage.addJobs(curSocket);
                }

                // Riduco il carico CPU dovuto al POOL mettendo in pausa (1/10/100 ms) il worker.
                // Lo scheduler in questo modo potrà ri-allocare risorse per altri thread/processi.
                // Senza il carico del server sulla CPU è circa 90%, con la pausa solo 0/1% (uso medio della demo).
                Thread.sleep(1);
            }
        }
        catch(Exception e)
        {
            // Stampo errori vari.
            System.out.println(" SERVER: Thread-Worker out of loop... [ ID" + threadID + " ]");
        }
    }
}

// --------------------------------------------------------------------------------------- //
//  End of File
// --------------------------------------------------------------------------------------- //
