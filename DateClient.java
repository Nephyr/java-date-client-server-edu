// --------------------------------------------------------------------------------------- //
//
//  @Author:        Vanzo Luca Samuele
//  @Version:       1.0.100
//  @Revision:      1
//
//  Semplice applicativo JAVA Client per connessione e stampa di data e frasi.
//  Codice di esempio per il corso "Reti e Sistemi Operativi".
//
// --------------------------------------------------------------------------------------- //

import java.net.*;
import java.io.*;
import java.util.*;

// --------------------------------------------------------------------------------------- //
//  Classe entry-point
// --------------------------------------------------------------------------------------- //

public class DateClient
{
    // ENTRY POINT PROGRAMMA
    public static void main(String[] args)
    {
        try
        {
            // Stampo intestazione inutilmente utile (è un ossimoro) XD
            System.out.println("\n Applicativo SO & Reti - JAVA Client (Demo App.)");
            System.out.println(" Produttore: Vanzo Luca Samuele, U14 Univ. Milano Bicocca - 2015\n");

            // Inizializzazione del classe INPUT (semplificata).
            Scanner keyboard    = new Scanner(System.in);

            // Connessione al server su porta 8000 (127.0.0.1 = LOCALHOST, non sempre è presente come alias!)
            // L'inizializzazione del socket causa un blocco temporaneo del programma,
            // in caso si mancata connessione l'esecuzione dello stesso termina con una socket exception.
            // Ottengo il canale di INPUT dal socket.
            Socket sock         = new Socket("127.0.0.1", 8000);
            InputStream in      = sock.getInputStream();

            // Inizializzazione canali di INPUT/OUTPUT del socket.
            BufferedReader bin  = new BufferedReader(new InputStreamReader(in));
            PrintWriter pout    = new PrintWriter(sock.getOutputStream(), true);

            // Inizializzo buffer STRING per comandi utente.
            String line         = "";

            // Stampo info menù e scelte.
            System.out.println("\n Possibili richieste verso il server Java:\n\n" +
                               "     - Digita [1] per richiedere la data corrente\n" +
                               "     - Digita [2] per richiedere una frase\n" +
                               "     - Digita [0] per chiudere il programma.\n");

            // Ciclo di vita del THREAD.
            while(true)
            {
                // Stampo info INPUT.
                System.out.print("\n Immettere un numero:  ");

                // Ottengo l'INPUT utente come STRINGA (Ottengo una linea alla volta, ENTER = \n = termine lettura).
                line = keyboard.nextLine();

                // Accedo allo scopo in presenza della selezione UNO (1).
                if(line.compareTo("1") == 0)
                {
                    // Invio richiesta data ed ora al server.
                    pout.println("GET_DATE");

                    // Stampo il messaggio relativo al MSG_OPCODE.
                    System.out.println(" RESULT:  " + bin.readLine());
                }
                // Accedo allo scopo in presenza della selezione DUE (2).
                else if(line.compareTo("2") == 0)
                {
                    // invio richiesta frase al server.
                    pout.println("GET_PHRASE");

                    // Stampo il messaggio relativo al MSG_OPCODE.
                    System.out.println(" RESULT:  " + bin.readLine());
                }
                // Accedo allo scopo in presenza della selezione ZERO (0).
                else if(line.compareTo("0") == 0)
                {
                    // Invio chiusura della connessione al server.
                    // Chiudo la socket lato client.
                    pout.println("QUIT");
                    sock.close();

                    // Stampo il messaggio relativo al MSG_OPCODE.
                    System.out.println(" RESULT:  Client disconnesso!");
                    break;
                }
                // Accedo allo scopo in presenza di scelte non gestite.
                else
                {
                    // Invio MSG_OPCODE per scelta non gestita.
                    pout.println("UNKWN");

                    // Stampo il messaggio relativo al MSG_OPCODE.
                    System.out.println(" RESULT:  " + bin.readLine());
                }
            }
        }
        catch(Exception e)
        {
            // Stampo eventuali errori del programma.
            System.out.println(" ERROR:  Client disconnesso dal Server remoto!");
        }
    }
}

// --------------------------------------------------------------------------------------- //
//  End of File
// --------------------------------------------------------------------------------------- //
